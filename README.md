# Nombre del Proyecto: Microservicios de Productos y Pedidos

Este proyecto consiste en dos microservicios interconectados: uno para la gestión de productos y otro para la gestión de pedidos. Ambos microservicios están conectados a una base de datos MySQL utilizando Hibernate como ORM (Object-Relational Mapping). El objetivo principal de este proyecto es permitir la creación de pedidos, actualizando el stock de productos y calculando el precio total del pedido utilizando el precio de los productos correspondientes.

## Descripción del Proyecto

El proyecto se compone de dos microservicios, ambos con sus respectivas funciones RESTful:

### Microservicio de Productos

Este microservicio se encarga de gestionar los productos. Proporciona funcionalidades como:

- Consultar la lista de productos disponibles.
- Actualizar el stock de un producto después de un pedido.
- Recuperar el precio de un producto específico.

### Microservicio de Pedidos

Este microservicio se encarga de gestionar los pedidos. Ofrece las siguientes funcionalidades:

- Crear un nuevo pedido, proporcionando la información necesaria, como el cliente y los productos solicitados.
- Llamar al microservicio de Productos para actualizar el stock y recuperar el precio de los productos relacionados con el pedido.
- Calcular el precio total del pedido y almacenar toda su información en la base de datos.

El microservicio de Pedidos y el microservicio de Productos se comunican a través de RestTemplate para realizar las solicitudes HTTP entre los microservicios.

## Tecnologías Utilizadas

El proyecto ha sido desarrollado utilizando las siguientes tecnologías y herramientas:

- Java: Lenguaje de programación utilizado para implementar la lógica de los microservicios.
- Spring: Framework de desarrollo de aplicaciones Java utilizado para construir aplicaciones empresariales.
- Hibernate JPA: Implementación de la especificación JPA (Java Persistence API) utilizada para el mapeo objeto-relacional y la interacción con la base de datos.
- Eclipse: Entorno de desarrollo integrado (IDE) utilizado para desarrollar y administrar el proyecto.
- MySQL: Sistema de gestión de bases de datos relacional utilizado para almacenar la información de productos y pedidos.
- RESTful API: Arquitectura utilizada para la comunicación entre los microservicios, basada en el protocolo HTTP.
- RestTemplate: Biblioteca de Spring utilizada para realizar solicitudes HTTP entre los microservicios.

## Configuración del Proyecto

Para configurar y ejecutar el proyecto en tu entorno local, sigue los pasos a continuación:

1. Clona este repositorio en tu máquina local o descárgalo como archivo ZIP.
2. Abre el proyecto en Eclipse o importa los microservicios en tu IDE preferido.
3. Asegúrate de tener un servidor MySQL instalado y configurado con una base de datos adecuada.
4. Configura las conexiones a la base de datos en los archivos de configuración correspondientes (por ejemplo, `application.properties`), proporcionando la URL de la base de datos, el nombre de usuario y la contraseña.
5. Compila y ejecuta cada microservicio por separado. Asegúrate de que no haya errores.
6. Los microservicios estarán disponibles en las URL correspondientes, por ejemplo, `http://localhost:8000/productos` para el microservicio de Productos y `http://localhost:9000/pedidos` para el microservicio de Pedidos.

## Contribución

Si deseas contribuir a este proyecto, puedes seguir los pasos a continuación:

1. Realiza un fork de este repositorio y clónalo en tu máquina local.
2. Crea una rama nueva para realizar tus modificaciones: `git checkout -b feature/nueva-caracteristica`.
3. Realiza las modificaciones y realiza confirmaciones (commits) claros para describir tus cambios.
4. Publica tus cambios en tu repositorio remoto: `git push origin feature/nueva-caracteristica`.
5. Crea una solicitud de extracción (pull request) en este repositorio y describe tus modificaciones detalladamente.

Todas las contribuciones son bienvenidas. Cualquier corrección de errores, mejoras de funcionalidad o nuevas características serán apreciadas.

## Contacto

Si tienes alguna pregunta, sugerencia o problema relacionado con el proyecto, no dudes en contactar al equipo de desarrollo a través del siguiente correo electrónico: [mielectronico@example.com](mailto:tu-correo-electronico@example.com).
