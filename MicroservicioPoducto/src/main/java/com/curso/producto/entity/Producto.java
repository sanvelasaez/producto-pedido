package com.curso.producto.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "producto")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer codigoProducto;
	private String nombreProducto;
	private Double precioUnitario;
	private Integer stock;

	/**
	 * 
	 */
	public Producto() {
	}

	/**
	 * @param codigoProducto
	 * @param nombreProducto
	 * @param precioUnitario
	 * @param stock
	 */
	public Producto(Integer codigoProducto, String nombreProducto, Double precioUnitario, Integer stock) {
		this.codigoProducto = codigoProducto;
		this.nombreProducto = nombreProducto;
		this.precioUnitario = precioUnitario;
		this.stock = stock;
	}

	/**
	 * @return the codigoProducto
	 */
	public Integer getCodigoProducto() {
		return codigoProducto;
	}

	/**
	 * @param codigoProducto the codigoProducto to set
	 */
	public void setCodigoProducto(Integer codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	/**
	 * @return the nombreProducto
	 */
	public String getNombreProducto() {
		return nombreProducto;
	}

	/**
	 * @param nombreProducto the nombreProducto to set
	 */
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	/**
	 * @return the precioUnitario
	 */
	public Double getPrecioUnitario() {
		return precioUnitario;
	}

	/**
	 * @param precioUnitario the precioUnitario to set
	 */
	public void setPrecioUnitario(Double precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	/**
	 * @return the stock
	 */
	public Integer getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Producto [codigoProducto=" + codigoProducto + ", nombreProducto=" + nombreProducto + ", precioUnitario="
				+ precioUnitario + ", stock=" + stock + "]";
	}

}
