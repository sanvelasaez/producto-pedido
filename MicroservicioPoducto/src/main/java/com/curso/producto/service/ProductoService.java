package com.curso.producto.service;

import java.util.List;

import com.curso.producto.entity.Producto;
import com.curso.producto.model.Pedido;

public interface ProductoService {

	List<Producto> dameProductos();
	Producto buscarProducto(Integer codigoProducto);
	void altaProducto(Producto producto);
	void actualizarProducto(Producto producto);
	void eliminarProducto(Integer codigoProducto);
	
	void actualizaStockProducto(Integer codProducto,Integer stock);
	Double damePrecioProducto(Integer codigoProducto);
	List<Pedido> damePedidos(Integer codigoProducto);//recibir todos los pedidos que tengan este producto
}
