package com.curso.producto.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.curso.producto.entity.Producto;
import com.curso.producto.model.Pedido;
import com.curso.producto.repository.ProductoDAO;

@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	RestTemplate template;

	@Autowired
	ProductoDAO dao;

	private String urlPedidos = "http://localhost:9000/pedidos/";

	@Override
	public List<Producto> dameProductos() {
		return dao.findAll();
	}

	@Override
	public Producto buscarProducto(Integer codigoProducto) {
		return dao.findById(codigoProducto).orElse(null);
	}

	@Override
	public void altaProducto(Producto producto) {
		dao.save(producto);
	}

	@Override
	public void actualizarProducto(Producto producto) {
		dao.save(producto);
	}

	@Override
	public void eliminarProducto(Integer codigoProducto) {
		dao.deleteById(codigoProducto);
	}

	@Override
	public void actualizaStockProducto(Integer codProducto, Integer unidades) {
		Producto producto = buscarProducto(codProducto);
		producto.setStock(producto.getStock() - unidades);
		actualizarProducto(producto);
	}

	@Override
	public Double damePrecioProducto(Integer codigoProducto) {
		return buscarProducto(codigoProducto).getPrecioUnitario();
	}

	@Override
	public List<Pedido> damePedidos(Integer codigoProducto) {
		return template.getForObject(urlPedidos + "producto/" + codigoProducto, List.class);
	}

}
