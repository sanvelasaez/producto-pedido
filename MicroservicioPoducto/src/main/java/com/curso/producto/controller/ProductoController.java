package com.curso.producto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.producto.entity.Producto;
import com.curso.producto.model.Pedido;
import com.curso.producto.service.ProductoService;

@RestController
@RequestMapping("/productos") // http://localhost:8000/productos
public class ProductoController {

	@Autowired
	ProductoService service;

	@GetMapping
	public ResponseEntity<List<Producto>> listarProductos() {
		List<Producto> productos = service.dameProductos();
		if (productos.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(productos);
	}

	@GetMapping("/{codigoProducto}")
	public ResponseEntity<Producto> buscaProducto(@PathVariable Integer codigoProducto) {
		return ResponseEntity.ok(service.buscarProducto(codigoProducto));
	}

	@PostMapping
	public void altaProducto(@RequestBody Producto producto) {
		service.altaProducto(producto);
	}

	@PutMapping
	public void actualizarProducto(@RequestBody Producto producto) {
		service.actualizarProducto(producto);
	}

	@DeleteMapping("/{codigoProducto}")
	public void eliminarProducto(@PathVariable Integer codigoProducto) {
		service.eliminarProducto(codigoProducto);
	}

	@GetMapping("/precio/{codigoProducto}")
	public ResponseEntity<Double> damePrecioProducto(@PathVariable Integer codigoProducto) {
		return ResponseEntity.ok(service.damePrecioProducto(codigoProducto));
	}

	@PutMapping(params = { "codigoProducto", "unidades" })
	public void actualizaStockProducto(@RequestParam("codigoProducto") Integer codigoProducto,
			@RequestParam("unidades") Integer unidades) {
		service.actualizaStockProducto(codigoProducto, unidades);
	}

	@GetMapping("/pedidos/{codigoProducto}")
	public ResponseEntity<List<Pedido>> buscaPedidos(@PathVariable Integer codigoProducto) {
		return ResponseEntity.ok(service.damePedidos(codigoProducto));
	}
}
