package com.curso.pedido.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.pedido.entity.Pedido;

@Repository
public interface PedidoDAO extends JpaRepository<Pedido, Integer>{

}
