package com.curso.pedido.service;

import java.util.List;

import com.curso.pedido.entity.Pedido;

public interface PedidoService {

	List<Pedido> damePedidos();
	Pedido buscarPedido(Integer idPedido);
	void altaPedido(Pedido producto);
	void actualizarPedido(Pedido producto);
	void eliminarPedido(Integer idPedido);
	
	Double damePrecioProducto(Integer codigoProducto);
	List<Pedido> buscarPedidosPorProducto(Integer codigoProducto);
	
}
