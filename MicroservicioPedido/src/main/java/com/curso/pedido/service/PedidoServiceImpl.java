package com.curso.pedido.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.curso.pedido.entity.Pedido;
import com.curso.pedido.repository.PedidoDAO;

@Service
public class PedidoServiceImpl implements PedidoService {

	@Autowired
	RestTemplate template;

	@Autowired
	PedidoDAO dao;

	String urlProductos = "http://localhost:8000/productos";

	@Override
	public List<Pedido> damePedidos() {
		return dao.findAll();
	}

	@Override
	public Pedido buscarPedido(Integer codigoProducto) {
		return dao.findById(codigoProducto).orElse(null);
	}

	@Override
	public void altaPedido(Pedido pedido) {
		template.put(
				urlProductos + "?codigoProducto=" + pedido.getCodigoProducto() + "&unidades=" + pedido.getUnidades(),
				Pedido.class);
		Double precioProducto = damePrecioProducto(pedido.getCodigoProducto());
		pedido.setTotal(pedido.getUnidades() * precioProducto);
		dao.save(pedido);
	}

	@Override
	public void actualizarPedido(Pedido producto) {
		dao.save(producto);
	}

	@Override
	public void eliminarPedido(Integer pedido) {
		dao.deleteById(pedido);
	}

	@Override
	public Double damePrecioProducto(Integer codigoProducto) {
		return template.getForObject(urlProductos + "/precio/" + codigoProducto, Double.class);
	}

	@Override
	public List<Pedido> buscarPedidosPorProducto(Integer codigoProducto) {
		List<Pedido> pedidos = new ArrayList();
		for (Pedido pedido : damePedidos()) {
			if (Objects.equals(pedido.getCodigoProducto(), codigoProducto))
				pedidos.add(pedido);
		}
		return pedidos;
	}

}
