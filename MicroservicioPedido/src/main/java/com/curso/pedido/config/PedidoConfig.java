package com.curso.pedido.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class PedidoConfig {

	@Bean
	RestTemplate getTemplate() {
		return new RestTemplate();
	}
}
