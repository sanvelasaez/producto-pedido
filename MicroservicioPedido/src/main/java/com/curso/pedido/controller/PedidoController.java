package com.curso.pedido.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.pedido.entity.Pedido;
import com.curso.pedido.service.PedidoService;

@RestController
@RequestMapping("/pedidos") // http://localhost:9000/pedidos
public class PedidoController {

	@Autowired
	PedidoService service;

	@GetMapping
	public ResponseEntity<List<Pedido>> listar() {
		List<Pedido> pedidos = service.damePedidos();
		if (pedidos.isEmpty())
			return ResponseEntity.noContent().build();
		return ResponseEntity.ok(pedidos);
	}

	@GetMapping("/{idPedido}")
	public ResponseEntity<Pedido> buscar(@PathVariable Integer idPedido) {
		return ResponseEntity.ok(service.buscarPedido(idPedido));
	}

	@PostMapping
	public void alta(@RequestBody Pedido pedido) {
		service.altaPedido(pedido);
	}

	@PutMapping
	public void actualizar(@RequestBody Pedido pedido) {
		service.actualizarPedido(pedido);
	}

	@DeleteMapping("/{idPedido}")
	public void eliminar(@PathVariable Integer idPedido) {
		service.eliminarPedido(idPedido);
	}
	
//	@GetMapping("/precio/{idPedido}")
//	public ResponseEntity<Double> damePrecioProducto(@PathVariable Integer idPedido) {
//		return ResponseEntity.ok(service.damePrecioProducto(idPedido));
//	}
	
//	@GetMapping("/producto/{codProducto}")
//	public ResponseEntity<List<Pedido>> buscarPedidos(@PathVariable Integer codProducto) {
//		return ResponseEntity.ok(service.buscarPedidosPorProducto(codProducto));
//	}
}
